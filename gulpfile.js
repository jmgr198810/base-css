var gulp = require("gulp");
var sass = require("gulp-sass");
var rename = require('gulp-rename');
//var minify = require('gulp-minify');

var tepjf = {
    sources : [
        "./development/sass/tepjf/init.scss"
    ],
    outputFile : "tepjf.css",
    outputDirectory : "./dist/css"
};

var site  = {
    sources : [
        "./development/sass/site/init.scss"
    ],
    outputFile : "site.css",
    outputDirectory : "./dist/css"
};

/* Tarea para ejecutar el proceso de compilación para codigo base */
gulp.task("css-tepjf", function(){
    return gulp.src(tepjf.sources)
    .pipe(sass(
        //{outputStyle: 'compressed'}
    ).on('error', sass.logError))
    .pipe(rename(tepjf.outputFile))
    .pipe(gulp.dest(tepjf.outputDirectory));
});

/* Tarea para ejecutar el proceso de compilación de cada sitio*/
gulp.task("css-site", function(){
    return gulp.src(site.sources)
    .pipe(sass(
        //{outputStyle: 'compressed'}
    ).on('error', sass.logError))
    .pipe(rename(site.outputFile))
    .pipe(gulp.dest(site.outputDirectory));
});

/* Tarea para ejecutar el proceso de compilación de boostrap*/
gulp.task("boostrap", function(){
    return gulp.src("./development/sass/boostrap.scss")
    .pipe(sass(
        {outputStyle: 'compressed'}
    ).on('error', sass.logError))
    .pipe(rename("boostrap.min.css"))
    .pipe(gulp.dest("./dist/css"));
});




// Se ejecuta la tarea por defecto
gulp.task('default', ['css-tepjf', 'css-site', 'boostrap']);

// Se observan los archivos para compilar
gulp.watch('./development/sass/**/*.scss', ['default']);
